/**
 * Created by betha on 12/20/2016.
 */
if (Meteor.isServer) {

    Meteor.methods({
        deleteMessage: function (data) {
            if ( data.deleteMsg.hasOwnProperty('msg_id') && data.deleteMsg.hasOwnProperty('owner_id')){

                var msg_id =data.deleteMsg.msg_id;
                var owner_id =data.deleteMsg.owner_id;

                var record = Messages.findOne({_id:msg_id});

                if(typeof record!=="undefined"){
                    if(record.owner_id ===owner_id){
                       var result = Messages.remove({_id:msg_id});

                       var rtrn = '{"status": "Success","Message":"Message was deleted successfully"}';
                       return rtrn;
                    }
                    else{
                        var rtrn = '{"status": "Failed","Message":"You are not Authorized to delete the message."}';
                        return rtrn;
                    }
                }
                else{
                    var rtrn = '{"status": "Failed","Message":"No record not found."}';
                    return rtrn;
                }
            }else{
                var rtrn = '{"status": "Error" , "Message": "Required parameters missing"}';
                return rtrn;
            }
        }
    });
}