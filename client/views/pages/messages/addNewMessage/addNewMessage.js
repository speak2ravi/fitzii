/**
 * Created by betha on 12/20/2016.
 */
Template.addNewMessage.events({

    'submit form': function(event){
        event.preventDefault();

        var owner_id = Session.get("owner_id");
        var msgText = event.target.messageText.value;

        Messages.insert({owner_id:owner_id,msg_text:msgText});
        Session.setPersistent('mode',"view");

    },
    'click #btn_cancel':function(){
        Session.setPersistent('mode',"view");
    }
});