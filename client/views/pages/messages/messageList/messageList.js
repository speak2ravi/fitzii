/**
 * Created by betha on 12/20/2016.
 */
MessagseSub = new SubsManager({cacheLimit:1});

Template.messageList.onCreated(function(){
    var instance = this;

    instance.messageListReady = new ReactiveVar();
    instance.rows = new ReactiveVar();

    Session.setPersistent('mode',"View");

    this.autorun(function () {
        var handleMessageList = MessagseSub.subscribe('messages',instance.rows.get());
        instance.messageListReady.set(handleMessageList.ready());
    });
});

Template.messageList.helpers({
    Messages:function(){
        return Messages.find({},{sort:{created_at:-1}});
    },

    isAddMode:function(){
       var mode = Session.get('mode');

       if(mode==="Add"){
           return true;
       }else{
           return false;
       }
    }
});


Template.messageList.events({
    'click #btn_add':function(){
        Session.setPersistent('mode',"Add");
    },


})

Template.message.events({
    'click #btn_delete':function(){

        var msg_id = this._id;
        var owner_id = Session.get("owner_id");

        var deleteMsg = {
            msg_id: msg_id,
            owner_id: owner_id
        };

        Meteor.call('deleteMessage', {deleteMsg: deleteMsg}, function (err, response) {
            if (err) {
                alert("Error" + err.message);
            }
            else{
                var result = JSON.parse(response);
                
                if(result.status !=="Success"){
                    alert(result.Message);
                }

                Session.setPersistent('mode',"View");

            }
        });
    }
})
